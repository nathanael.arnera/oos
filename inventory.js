class Inventory {
    constructor() {
      this.db = [];
    }
    reset(){
        this.db = []
    }
    findDepot(input) {
        let update = {}
        this.db.forEach(function(kitchen){
            if(kitchen.name === input.name){
                update = kitchen
            }
        })
        return update 
    }
    findOrInit(input){
        var oneDepot = this.findDepot(input)
        if(!oneDepot.hasOwnProperty('name')){
            console.log('is not saved')
            return this.initDepot(input.name)
        }else{
            return oneDepot
        }
    }
    initDepot(depotName){
        const depot =  {
            name : 	 depotName,
            soir :    "EMPTY",
            viande :  "EMPTY",
            veggie :  "EMPTY",
            poisson : "EMPTY",
            marche :  "EMPTY",
            tarte :  "EMPTY",
            chef :  "EMPTY",
            petitMarche :  "EMPTY"	,
            equilibre :  "EMPTY"	,
            equipe :  "EMPTY",
            pequilibre :  "EMPTY"	 	        
        }
        this.db.push(depot)
        return depot
    }
    update(depot, type , status){
        console.log("update")
        for (let i = 0; i < this.db.length; i++) {
            if( this.db[i].name === depot){
                this.db[i][type]= status
            }
        }
    }
}

module.exports = {
    Inventory
}