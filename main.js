const CronJob = require('cron').CronJob;
const express = require('express');
const request = require('request');

const alerts = require('./alerts');
const Inventory = require('./inventory').Inventory;

const slack = require('slack')

const allDepot = [{ 
        name : "nord", id : 1, weekend :false,  color: "#d0e894"
    },{ 
        name : "sud",id : 2,weekend :true,  idUrl : "e6dabd00-3d00-4173-870d-e92160ee392f", color:"#1b4d66"
    },{ 
        name : "ouest",id : 3,weekend :true,  idUrl : "3830534a-f928-491d-acad-a9524bcd840d", color :"#ad8e69"
    },{ 
        name : "levallois", id : 4,weekend :true,  idUrl : "18dea6f5-d32c-442f-9a28-e98dbd1a7664", color:"#4C3A4A"
    },{ 
        name : "ernestine", id : 5,weekend :true,  idUrl : "f0b85617-2b54-47b6-a3e8-b25969ddbf87", color : "#D61C61"
    },{ 
        name : "boulbi", id : 15,weekend :true,  color : "#D61C61"
    },{ 
        name : "miollis", id : 16,weekend :true,  color : "#A61D61"
    },{ 
        name : "wework", id : 14,weekend :false,  color : "#FFFF00"
    }]
  

    
/*  ROUTER */

var app = express();
/* Display current stock */
app.post('/stock', function (req, res) {
    allDepot.forEach(function(element){
        var memory = inventory.findOrInit( {name : element.name} )
        alerts.sendMessage.allInventory(memory , element)
    })
    res.send("let's see");
});

/* Reset current stock */
app.post('/reset', function (req, res) {
    inventory.reset()
    res.send("Recommencons :-) ! ");
});

app.listen(8080, function () {
    console.log('App listening on port 8080!');
});


/* INIT inventory */
var inventory = new Inventory()

/* CRON JOB */

/* Launch job every X min */
let job = new CronJob({
    cronTime: '*/5 * * * *',
    onTick: function() {
      launchSearch()
    },
    start: true,
});
job.start()



/* Check data for each Kitchen between 9 am et 11 pm */
function launchSearch (){
    let now = new Date()
    let hours = now.getHours() + 2

    if( hours>= 6 && hours < 23 ){
        allDepot.forEach(function( depot ) {
            checkDepot( depot )
        });
    }else{
        console.log("Not during the shift")
    }
}







/* From API call to an array of "formule" & "plateaux repas" 
* Then for each element of the array check the inventory level
*/
const   isAM = function ( date ) { return ( date.getHours() < 15 ) };
const   isWeekEnd = function ( date ) { return ( date.getDay() < 1 ||   date.getDay() > 5  ) };

function formatDate(){
    let date = new Date()
    return {
        am : isAM(date) ,
        pm : ! isAM(date),
        weekend : isWeekEnd(date), 
    }
}

function checkDepot( depot ){
    request.call('GET','https://api-gateway.frichti.co/menu/'+depot.id,{}, function( error, response, body ) {
        const data = formatData( body ) 
        data.forEach(function( element ){
            console.log("checkInventoryLevel")
            let actualShift = formatDate()
                checkInventoryLevel( element, depot )
     
        })
    })
}
const   isFormuleDuSoir = function ( element ) { return ( ( element.product ) ? element.product.title === 'Formule du Soir'   : false ) };
const   isFormuleMarche = function ( element ) { return ( ( element.product ) ? element.product.title === 'Formule du marché' : false ) };
const   isPetiteFormuleMarche = function ( element ) { return ( ( element.product ) ? element.product.title === "P'tite formule du marché" : false ) };
const   isFormuleEquilibre = function ( element ) { return ( ( element.product ) ? element.product.title === "Formule équilibre" : false ) };

const   isFormulePasSiTarte = function ( element ) { return ( ( element.product ) ? element.product.title === 'Formule Pas si tarte' : false ) };

const   isEntrees       = function ( element ) { return ( ( element ) ? element.name          === 'entrees'           : false ) };
const   isPlateauxRepas = function ( element ) { return ( ( element ) ? element.name          === 'nos-plateaux-repas': false ) }
const   isPlateauxRepasViande = function ( element ) { return ( ( element.product ) ? element.product.title === 'Plateau-repas viande': false ) }
const   isPlateauxRepasVeggie = function ( element ) { return ( ( element.product ) ? element.product.title === 'Plateau-repas veggie': false ) }
const   isPlateauxRepasPoisson = function ( element ) { return ( ( element.product ) ? element.product.title === 'Plateau-repas poisson': false ) }
const   isPlateauxRepasEquilibre = function ( element ) { return ( ( element.product ) ? element.product.title === 'Plateau-repas Équilibre': false ) }
const   isFormuleDequipe = function ( element ) { return ( ( element.product ) ? element.product.title === "Formule déj' d'équipe": false ) }


function formatData ( body ){
    var data = JSON.parse( body ) 
    var entrees =       data.categories.find( isEntrees )
    let plateaux =      data.categories.find( isPlateauxRepas )
    let formuleMarche = entrees.items.find( isFormuleMarche )
    let petiteFormuleMarche = entrees.items.find( isPetiteFormuleMarche )
    let formulePasSiTarte = entrees.items.find( isFormulePasSiTarte )
    let formuleDuSoir = entrees.items.find(   isFormuleDuSoir )
    let formuleEquilibre = entrees.items.find(   isFormuleEquilibre )
    let  plateauxViande = plateaux.items.find(isPlateauxRepasViande)
    let  plateauxVeggie= plateaux.items.find(isPlateauxRepasVeggie)
    let  plateauxPoisson = plateaux.items.find(isPlateauxRepasPoisson)
    let  plateauxEquilibre= plateaux.items.find(isPlateauxRepasEquilibre)
    let  formuleEquipe= plateaux.items.find(isFormuleDequipe)
    console.log("plateauxEquilibre")
    console.log(plateauxEquilibre)
    return [
        { 
        
            inventoryLevel : ( (formuleEquilibre )  ?  formuleEquilibre.product.inventoryLevel : "EMPTY"),
            name :  "Formule d'équilibre :carrot:",
            type :  "equilibre",
            weekend :true, 
            pm : true,
            am : true,
            id : 1000710
        },{ 
        
        inventoryLevel : ( (formuleEquipe )  ?  formuleEquipe.product.inventoryLevel : "EMPTY"),
        name :  "Formule d'équipe :busts_in_silhouette:",
        type :  "equipe",
        weekend :true, 
        pm : true,
        am : true,
        id : 1000717
    },{ 
        
        inventoryLevel : ( (petiteFormuleMarche )  ?  petiteFormuleMarche.product.inventoryLevel : "EMPTY"),
        name :  "Petite formule du marché :female-farmer:",
        type :  "petitMarche",
        weekend :true, 
        pm : true,
        am : true,
        id : 1000716
    },{ 
        
        inventoryLevel : ( (plateauxEquilibre )  ?  plateauxEquilibre.product.inventoryLevel : "EMPTY"),
        name :  "Plateau-repas équilibre :ok_hand:",
        type :  "pequilibre",
        weekend :true, 
        pm : true,
        am : true,
        id : 2000300
    },{ 
        inventoryLevel : ( (plateauxVeggie )  ?  plateauxVeggie.product.inventoryLevel : "EMPTY"),
        name :  "Plateau-repas veggie :avocado:",
        type :  "veggie",
        weekend :true, 
        pm : true,
        am : true,
        id : 1000554
    },{ 
        inventoryLevel :  (( plateauxPoisson)  ?  plateauxPoisson.product.inventoryLevel : "EMPTY"),
        name :  "Plateau-repas poisson 🐟",
        type :  "poisson",
        weekend :true, 
        pm : true,
        am : true,
        id : 1000553
    },{ 
        inventoryLevel : ( (plateauxViande )  ?  plateauxViande.product.inventoryLevel : "EMPTY"),
        name :  "Plateau-repas viande 🐄",
        type :  "viande",
        weekend :true, 
        pm : true,
        am : true,
        id : 1000552
    },{ 
        inventoryLevel : ( (formuleMarche) ?  formuleMarche.product.inventoryLevel : "EMPTY"),
        name :  "Formule du marché :female-farmer:",
        type :  "marche",
        weekend :true, 
        pm : false,
        am : true,
        id : 1000555
    },{ 
        inventoryLevel :  ( ( formuleDuSoir ) ?  formuleDuSoir.product.inventoryLevel : "EMPTY"),
        name :  "Formule du soir :crescent_moon:",
        type :  "soir",
        weekend :true, 
        pm : true,
        am : false,
        id : 1000653
    },
    { 
        inventoryLevel :  ( ( formulePasSiTarte ) ?  formulePasSiTarte.product.inventoryLevel : "EMPTY"),
        name :  "Formule Pas si tarte :pie:",
        type :  "tarte",
        weekend :true, 
        pm : false,
        am : true,
        id  : 1000682
    }]
}


/* 
* If the actual inventory state is different from the previous state 
* Then send an alert via slack
* And update memory
*/
function checkInventoryLevel(element, depot){
    var memory = inventory.findOrInit( {name : depot.name} )
    const actualState = element.inventoryLevel
    const previousState = memory[element.type]

    switch(actualState){
        case "AVAILABLE" :
            if(previousState === "OOS"){
               // alerts.sendMessage.backInStock( element, depot )
            }
            inventory.update(depot.name,  element.type, actualState )
            break
        case "LOW" :
            if(previousState != "LOW"){
               // alerts.sendMessage.lowInStock( element, depot )
            }
            inventory.update(depot.name,  element.type, actualState )
            break 
        case "OOS" :
            if(previousState != "OOS"){
                let actualShift = formatDate()
                console.log(actualShift)
                console.log(element)
                console.log(actualShift.am === element.am )
                console.log(actualShift.pm === element.pm)
                console.log(actualShift.weekend === element.weekend)
                if(((actualShift.am === element.am )  || (actualShift.pm === element.pm)) || (actualShift.weekend === element.weekend ) ){
                  //  alerts.sendMessage.outOfStock( element, depot )
                }else{
                    console.log("Not available for this shift")
                }

                
            }
            alerts.sendMessage.outOfStock( element, depot )

            inventory.update(depot.name,  element.type, actualState )
            break 
    }
}

