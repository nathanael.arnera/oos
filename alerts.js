const token = "xoxp-7220035952-282694826066-347912895379-f2db44be6dd2a647cebac1a70a0dbdc4"
const channel = "CABU4S32T"
const slack = require('slack')
const text = " "
const tag =  "<!here> "
const sendMessage = {
    sendChannelMessage ( attachments){
        let params = {token, text, channel , attachments}
        slack.chat.postMessage( params )
    },
    backInStock ( element, depot ) { 
        const attachments   =  [{
            "author_name": "🎉 Back In Stock " + displayTime(),
            "fallback": "none",
            "color": depot.color,
            "title":  "Dépôt " + jsUcfirst(depot.name) ,
            "text": element.name ,
            "attachment_type": "default",
        }]

        this.sendChannelMessage( attachments )
    },
    lowInStock ( element,depot ) { 
        const attachments  =  [ {
            "author_name": "⏳ Low In Stock " + displayTime(),
            "fallback": "none",
            "color": depot.color,
            "title":  "Dépôt " +jsUcfirst(depot.name) ,
            "text":  element.name ,
            "attachment_type": "default",
        }]

        this.sendChannelMessage( attachments )
    },
    outOfStock ( element, depot ) { 
        let fields = setFields(element)

        const attachments = [ {
            "author_name": "🚨 Out Of Stock" + displayTime(),
            "fallback": "none",
            "color": depot.color,
            "callback_id": "none",
            "title":  "Dépôt " +jsUcfirst(depot.name) ,
            "text": tag + element.name ,
            "attachment_type": "default",
            "actions": [ {
                    "name": "recommend",
                    "text": "Mettre à jour",
                    "type": "button",
                    "url":  "http://admin.frichti.co/formules/"+ element.id +"/"+depot.id,
                }
            ]
        }]
        this.sendChannelMessage( attachments )
    },
    allInventory ( element, depot ) { 
        let fields = setFields(element)
        let attachments = [ {
            "author_name": "🚨 Inventory" + displayTime(),
            "fallback": "none",
            "color": depot.color,
            "callback_id": "none",
            "title":  "Dépôt " +jsUcfirst(depot.name) ,
            "attachment_type": "default",
            "fields": fields
        }]
        this.sendChannelMessage( attachments )
    }
}

function setFields(element){
    let formules = [{
        title : "Formule d'équilibre"  ,
        value : element.equilibre,
        short: true,
        id : 1000710
    },{
        title : "Formule d'équipe"  ,
        value : element.equipe,
        short: true,
        id : 1000717
    },{
        title : "Petite formule du marché"  ,
        value : element.petitMarche,
        short: true,
        id : 1000716
    },{
        title : "Plateau-repas équilibre"  ,
        value : element.pequilibre,
        short: true,
        id : 2000300
    },{
        title : "Formule du Soir"  ,
        value : element.soir,
        short: true,
        id : 1000653
    },{
        title : "Formule du Marché",
        value : element.marche,
        short: true,
        id : 1000555 
    },{
        title : "Formule du pas si tarte",
        value : element.tarte,
        short: true,
        id: 1000682
    },{
        title : "Plateau-repas viande" ,
        value : element.viande,
        short: true,
        id :1000552
    },{
        title : "Plateau-repas veggie" ,
        value : element.veggie ,
        short: true,
        id : 1000554
    },{
        title : "Plateau-repas poisson"  ,
        value : element.poisson,
        short: true,
        id : 1000553
    },{
        title : "Plateau-repas du chef"  ,
        value : element.chef,
        short: true,
        id : 1000717
    }]
    let fields = []
    formules.forEach(function(menu){
        if(menu.value != "EMPTY"){
            fields.push(menu)  
        }
    })
    return fields
}

function displayTime(){
    var date = new Date;
    var hours = date.getHours() + 2
	return " (" + hours +":" + date.getMinutes()  + ") "
}

function jsUcfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

module.exports = {
    sendMessage 
}